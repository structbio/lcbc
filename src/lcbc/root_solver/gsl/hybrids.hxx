#ifndef GSL_ROOT_SOLVER_F
#define GSL_ROOT_SOLVER_F
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>

template <unsigned int N, typename numeric_t>
class lcbc;

template <unsigned int N, typename numeric_t>
bool gslRootSolverHybrids( int(*func)(const gsl_vector*, void*, gsl_vector*), 
										 			 typename lcbc<N,numeric_t>::function_t* xinit, 
                     			 lcbc<N,numeric_t>* crank, 
                     			 numeric_t precision=numeric_t(1e-11) , 
                    			 unsigned int max_iter=500000
                   			){
  
  // convert xinit to gsl_vector
  gsl_vector *x = gsl_vector_alloc(6);
  for(unsigned int i = 0; i < 6; i++){
  	gsl_vector_set( x, i, (*xinit)(i) );
  } 
  
	// set up function
	gsl_multiroot_function F = {func, 6, crank};
	
	// set up the solver
	const gsl_multiroot_fsolver_type *T;
	T = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver *s;
	s = gsl_multiroot_fsolver_alloc(T, 6);
	gsl_multiroot_fsolver_set (s, &F, x);     

	// Start iterations
	int status;
	unsigned int iter = 0;	
	do{
		iter++;
		status = gsl_multiroot_fsolver_iterate(s);
     
		if(status){
			break;
    }
		status = gsl_multiroot_test_residual (s->f, precision);
	}
	while (status == GSL_CONTINUE && iter < max_iter);

	if(status == GSL_SUCCESS){
		for(unsigned int i = 0; i < 6; i++){
  		(*xinit)(i) = gsl_vector_get(s->x, i);
  	}

  	gsl_multiroot_fsolver_free(s);
		gsl_vector_free(x);
		return true;
  }
  
	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(x);
	return false;
}

#endif
