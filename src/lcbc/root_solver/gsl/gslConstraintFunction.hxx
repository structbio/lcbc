#ifndef GSL_CONSTRAINT_FUNCTION
#define GSL_CONSTRAINT_FUNCTION

#include <gsl/gsl_vector.h>

template <unsigned int N, typename numeric_t>
class lcbc;

template <unsigned int N, typename numeric_t>
int gslConstraintFunctionDH( const gsl_vector* x, void* c, gsl_vector* f ){
	// convert x to crankshaft::function_t
	typename lcbc<N,numeric_t>::function_t conf;
	for(unsigned int i = 0; i != 6; ++i){
		conf(i) = gsl_vector_get( x, i );
	}
	
	lcbc<N,numeric_t>* crank = (class lcbc<N,numeric_t>*)c;
	typename lcbc<N,numeric_t>::function_t func = crank->constraintFunctionDH( conf );
	
	// convert crankshaft::function_t to gsl_vector
	for(unsigned int i = 0; i != 6; ++i){
		gsl_vector_set( f, i, func(i) );
	}
	return GSL_SUCCESS;
};

template <unsigned int N, typename numeric_t>
int gslConstraintFunctionLocal( const gsl_vector* x, void* c, gsl_vector* f ){
	// convert x to crankshaft::function_t
	typename lcbc<N,numeric_t>::function_t conf;
	for(unsigned int i = 0; i != 6; ++i){
		conf(i) = gsl_vector_get( x, i );
	}
	
	lcbc<N,numeric_t>* crank = (class lcbc<N,numeric_t>*)c;
	typename lcbc<N,numeric_t>::function_t func = crank->constraintFunctionLocal( conf );
	
	// convert crankshaft::function_t to gsl_vector
	for(unsigned int i = 0; i != 6; ++i){
		gsl_vector_set( f, i, func(i) );
	}
	return GSL_SUCCESS;
};


#endif
