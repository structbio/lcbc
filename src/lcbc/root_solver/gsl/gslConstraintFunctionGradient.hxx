#ifndef CONSTRAINT_FUNCTION_GRADIENT
#define CONSTRAINT_FUNCTION_GRADIENT

#include <gsl/gsl_vector.h>
#include "../crankshaft.hxx"

template <unsigned int N, typename numeric_t>
int constraintFunctionGradientExactDH( gsl_vector* x, crankshaft<N,numetic_t>* c, gsl_vector* f, gsl_matrix* J ){
	// convert x to crankshaft::function_t
	typename crankshaft<N,numeric_t>::function_t conf;
	for(unsigned int i = 0; i != 6; ++i){
		conf(i) = gsl_vector_get( x, i );
	}
	
	typename crankshaft<N,numeric_t>::function_t func = c->constraintFunctionDH( conf );
	typename crankshaft<N,numeric_t>::reduced_gradient_t grad = c->gradientExactDH( conf );
	
	// convert crankshaft::function_t to gsl_vector
	for(unsigned int i = 0; i != 6; ++i){
		gsl_vector_set( f, i, func(i) );
		for(unsigned int j = 0; j != 6; ++j){
			gsl_matrix_set( J, i, j, grad(i,j) );
		}
	}
	return GSL_SUCCESS;
};

template <unsigned int N, typename numeric_t>
int constraintFunctionGradientForwardDifferenceDH( gsl_vector* x, crankshaft<N,numetic_t>* c, gsl_vector* f, gsl_matrix* J ){
	// convert x to crankshaft::function_t
	typename crankshaft<N,numeric_t>::function_t conf;
	for(unsigned int i = 0; i != 6; ++i){
		conf(i) = gsl_vector_get( x, i );
	}
	
	typename crankshaft<N,numeric_t>::function_t func = c->constraintFunctionDH( conf );
	typename crankshaft<N,numeric_t>::reduced_gradient_t grad = c->gradientForwardDifferenceDH( conf );
	
	// convert crankshaft::function_t to gsl_vector
	for(unsigned int i = 0; i != 6; ++i){
		gsl_vector_set( f, i, func(i) );
		for(unsigned int j = 0; j != 6; ++j){
			gsl_matrix_set( J, i, j, grad(i,j) );
		}
	}
	return GSL_SUCCESS;
};

template <unsigned int N, typename numeric_t>
int constraintFunctionGradientForwardDifferenceLocal( gsl_vector* x, crankshaft<N,numetic_t>* c, gsl_vector* f, gsl_matrix* J ){
	// convert x to crankshaft::function_t
	typename crankshaft<N,numeric_t>::function_t conf;
	for(unsigned int i = 0; i != 6; ++i){
		conf(i) = gsl_vector_get( x, i );
	}
	
	typename crankshaft<N,numeric_t>::function_t func = c->constraintFunctionLocal( conf );
	typename crankshaft<N,numeric_t>::reduced_gradient_t grad = c->gradientForwardDifferenceLocal( conf );
	
	// convert crankshaft::function_t to gsl_vector
	for(unsigned int i = 0; i != 6; ++i){
		gsl_vector_set( f, i, func(i) );
		for(unsigned int j = 0; j != 6; ++j){
			gsl_matrix_set( J, i, j, grad(i,j) );
		}
	}
	return GSL_SUCCESS;
};

#endif
