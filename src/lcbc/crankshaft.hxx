#ifndef GENERALIZED_CRANKSHAFT_HXX
#define GENERALIZED_CRANKSHAFT_HXX

#include <Eigen/Core>
#include <Eigen/QR>
#include <array>

#include "utils/tuple.hpp"
#include "dh_notation/dh_chain.hxx"
#include "root_solver/gsl/hybrids.hxx"
#include "root_solver/gsl/gslConstraintFunction.hxx"

// N is the number of degrees of freedom
template < unsigned int N, typename numeric_t >
class lcbc{
	public:
		// configuration typedefs
		typedef typename Eigen::Matrix< numeric_t, N, 1 > config_t;
		typedef typename Eigen::Matrix< numeric_t, 6, 1 > function_t;
		// gradient typedefs
		typedef typename Eigen::Matrix< numeric_t, 6, N > gradient_t;
		typedef typename Eigen::Matrix< numeric_t, 6, 6 > reduced_gradient_t;
		// basis typedefs
		typedef typename Eigen::Matrix< numeric_t, N, N-6 > tangent_basis_t;
		typedef typename Eigen::Matrix< numeric_t, N, 6 > orthogonal_basis_t;
		typedef typename Eigen::Matrix< numeric_t, N, N > complete_basis_t;
	protected:
		bool success;																													//!< true if the last method called exited successfully
		config_t config_dh;      					  																	//!< the actual configuration in Denavit-Hartenberg notation
		config_t config_local;																								//!< the actual configuration in local coordinate
				
		tangent_basis_t tbasis;  					  																	//!< a basis for the tangent space in the actual configuration
		orthogonal_basis_t obasis;																						//!< a basis that span the space orthogonal to the tangent space

		gradient_t gradient_dh;																								//!< gradient of constraint function computed at the actual configuration
		gradient_t gradient_local;																						//!< gradient of constraint function computed at the actual configuration
		DH::chain<N+1, numeric_t> chain;    																	//!< store the initial chain parameters ( alpha, r, and d )
		Eigen::Matrix< numeric_t, 4, 4 > M; 																	//!< the initial DH matrix describing the change of coordinate frame
		
		/* Utils */
		function_t configDHToFunctionDH( config_t& cfg );											//!< convert config_t to function_t
		function_t configLocalToFunctionLocal( config_t& cfg );								//!< convert config_t to function_t
		config_t configDHToConfigLocal( config_t& cfg );											//!< convert config_t to config_t
		config_t configLocalToConfigDH( config_t& cfg );											//!< convert config_t to config_t
		config_t functionLocalToConfigLocal( function_t& ftn );								//!< convert function_t to config_t
		config_t functionLocalToConfigDH( function_t& ftn );									//!< convert function_t to config_t
		/* The following methods return the constraint functions */
		function_t constraintFunctionLocal();																	//!< compute the values of the constraint functions
		function_t constraintFunctionLocal( config_t& cfg );									//!< compute the values of the constraint functions
		function_t constraintFunctionDH();																		//!< compute the values of the constraint functions
		function_t constraintFunctionDH( config_t& cfg );											//!< compute the values of the constraint functions
		
		/* The following methods return the derivative by using the implicit function theorem */
		bool implicitFunctionTheoremDerivative( tuple<N-6,N>& angles );
		bool implicitFunctionTheoremDerivative( config_t& cfg, tuple<N-6,N>& angles );
		
		/* The following methods compute the gradient of the constraint functions. */
		gradient_t& gradientExactDH();              													//!< compute the gradient by deriving exactly the DH matrix M. The basis is the DH basis
		gradient_t& gradientExactDH( config_t& cfg ); 												//!< compute the gradient by deriving exactly the DH matrix M. The basis is the DH basis

		gradient_t& gradientForwardDifferenceDH();  		             					//!< compute the gradient by using a forward difference approximation. The basis is the DH basis
		gradient_t& gradientForwardDifferenceDH( config_t& cfg );  						//!< compute the gradient by using a forward difference approximation. The basis is the DH basis
		gradient_t& gradientForwardDifferenceLocal();  		           					//!< compute the gradient by using a forward difference approximation. The basis is the local basis
		gradient_t& gradientForwardDifferenceLocal( config_t& cfg );					//!< compute the gradient by using a forward difference approximation. The basis is the local basis
		
		/* The following methods perform different operations on the gradient of the constraint functions */
		bool computeLocalBasis();    																					//!< compute the basis by using the implicit function theorem
		bool computeLocalBasis( config_t& cfg );															//!< compute the basis by using the implicit function theorem
		
	public:	
		lcbc( typename DH::chain<N, numeric_t>::raw_chain_t& input_chain );
		bool isSuccess();
		template <typename visitor_t>
		config_t step( config_t& direction, numeric_t ds, visitor_t& visitor );
		
		/* The following methods compute the reduced gradient of the constraint functions. The derivatives are taken with respect to only free angles */	
		reduced_gradient_t gradientExactLocal( function_t& ftn );							//!< compute the gradient by deriving exactly the DH matrix M
		reduced_gradient_t gradientForwardDifferenceLocal( function_t& ftn );	//!< compute the gradient by using a forward difference approximation
		
		/* The following methods compute the constraint functions. */	
		function_t constraintFunctionLocal( function_t& ftn );								//!< compute the values of the constraint functions
		function_t constraintFunctionDH( function_t& ftn );										//!< compute the values of the constraint functions
		
		/**/
};

/*** CONSTRUCTOR ***/
template < unsigned int N, typename numeric_t >
inline lcbc<N,numeric_t>::lcbc( typename DH::chain<N, numeric_t>::raw_chain_t& input_chain ){
	// initialize status flag
	success = true;
	// add a virtual edge at the end of the chain
	typename DH::chain<N+1, numeric_t>::raw_chain_t extended_chain;
	std::copy( input_chain.begin(),input_chain.end(),extended_chain.begin() );
	typename DH::edge<numeric_t>::vector_t v1 = input_chain[N-1].second - input_chain[N-1].first, v2 = input_chain[N-2].second - input_chain[N-2].first;
	extended_chain[N] = { input_chain[N-1].second, 2*input_chain[N-1].second + v2.cross(v1) - input_chain[N-1].first};
//	std::cout << v2.cross(v1) << std::endl;
	chain = DH::chain<N+1, numeric_t>(extended_chain);
	// compute the initial DH matrix
	M = chain.matrixCompute();
	// initialize actual configuration
	config_dh = chain.getTheta();
};

/*** STEP ***/
template < unsigned int N, typename numeric_t >
template <typename visitor_t>
typename lcbc<N,numeric_t>::config_t lcbc<N,numeric_t>::step( config_t& direction, numeric_t ds, visitor_t& visitor ){
	success = true;
	
	// normalize direction
	direction.normalize();

	// update the basis
	computeLocalBasis( );
	if(not success){
		visitor.onImplicitFunctionTheoremGradientIsNotInvertible();
		return config_dh;
	}

	// project direction onto the tangent space
	config_t projected_dir_local = config_t::Zero();
	for( unsigned int i = 0; i != N-6; ++i ){
		projected_dir_local(i) = tbasis.col(i).dot( direction );
	}

	// if direction is orthogonal to tangent space, exit
	if( projected_dir_local.norm() < numeric_t(1.0e-4) ){
		visitor.onDirectionIsOrthogonalToTangentSpace( tbasis, obasis, direction, projected_dir_local );
		return config_dh;
	}
	
	// rescale direction
	direction *= ds;
	projected_dir_local *= ds;
	
	// move along projected direction;
	config_local = projected_dir_local;
			
	// root solver procedure
	function_t x0 = function_t::Zero();
	config_t old_config = config_dh;
	
	if( gslRootSolverHybrids<N,numeric_t>( &gslConstraintFunctionLocal<N,numeric_t>, &x0, &(*this) ) ){
		config_local = functionLocalToConfigLocal( x0 );
		config_dh = configLocalToConfigDH( config_local );
		visitor.onRootFindingSuccess(tbasis, obasis, projected_dir_local, old_config, config_dh, config_local);
	}
	else{
		config_dh = old_config;
		visitor.onRootFindingConvergenceFault(tbasis, obasis, projected_dir_local, config_dh, config_local);
	}
	return config_dh;
}

/*** IS SUCCESS ***/
template < unsigned int N, typename numeric_t >
inline bool lcbc<N,numeric_t>::isSuccess(){
	return success;
};

/*** CONSTRAINT FUNCTION ***/
template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::function_t lcbc<N,numeric_t>::constraintFunctionLocal(){
	return constraintFunctionLocal(config_local);
}

template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::function_t lcbc<N,numeric_t>::constraintFunctionLocal( function_t& ftn ){
	config_t conf_dh = functionLocalToConfigDH( ftn );
	return constraintFunctionDH(conf_dh);
}

template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::function_t lcbc<N,numeric_t>::constraintFunctionLocal( config_t& cfg ){
	config_t conf_dh = configLocalToConfigDH(cfg);
	return constraintFunctionDH(conf_dh);
}

template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::function_t lcbc<N,numeric_t>::constraintFunctionDH(){
	return constraintFunctionDH(config_dh);
}

template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::function_t lcbc<N,numeric_t>::constraintFunctionDH( function_t& ftn ){
	config_t conf_dh = functionDHToConfigDH( ftn );
	return constraintFunctionDH(conf_dh);
}

template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::function_t lcbc<N,numeric_t>::constraintFunctionDH( config_t& cfg ){
	chain.setTheta(cfg);
	Eigen::Matrix<numeric_t,4,4> Mf = chain.matrixCompute() - M;
	function_t f;
	f << Mf(0,2), Mf(0,3), Mf(1,2), Mf(1,3), Mf(0,1), Mf(2,3);
	return f;
}

/*** GRADIENT EXACT ***/

template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::gradient_t& lcbc<N,numeric_t>::gradientExactDH(){
	return gradientExactDH(config_dh);
}

template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::gradient_t& lcbc<N,numeric_t>::gradientExactDH( config_t& cfg ){
	chain.setTheta(cfg);
//	std::cout << chain.matrixCompute() << std::endl << std::endl;
	for(unsigned int j=0; j<N; ++j){
		Eigen::Matrix< numeric_t, 4, 4 > Gamma = chain.matrixComputeFirstDerivative(DH::parameter_t::THETA, j);
//		std::cout << Gamma << std::endl;
		gradient_dh(0,j) = Gamma(0,2);
		gradient_dh(1,j) = Gamma(0,3);
		gradient_dh(2,j) = Gamma(1,2);
		gradient_dh(3,j) = Gamma(1,3);
		gradient_dh(4,j) = Gamma(0,1);
		gradient_dh(5,j) = Gamma(2,3);
	}
	return gradient_dh;
}

/*** GRADIENT APPROXIMATION ***/

template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::gradient_t& lcbc<N,numeric_t>::gradientForwardDifferenceDH(){
	return gradientForwardDifferenceDH(config_dh);
}

template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::gradient_t& lcbc<N,numeric_t>::gradientForwardDifferenceDH( config_t& cfg ){
	config_t xh = cfg;
	function_t f0 = constraintFunctionDH(xh);
	numeric_t temp, h, epsilon=numeric_t(1.0e-8);
	for (unsigned int j=0; j<N; ++j){
		xh = cfg;
		temp=xh(j);
		h=epsilon*abs(temp);
		if (h == 0.0){ 
			h=epsilon;
		}
		xh(j)=temp+h;
		h=xh(j)-temp;  // Trick to reduce finite-precision error.
		function_t f1 = constraintFunctionDH(xh);
		xh(j) = temp;
		gradient_dh.col(j) = (f1-f0)/h;
	}
	return gradient_dh;
}

template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::gradient_t& lcbc<N,numeric_t>::gradientForwardDifferenceLocal(){
	return gradientForwardDifferenceLocal(config_local);
}

template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::gradient_t& lcbc<N,numeric_t>::gradientForwardDifferenceLocal( config_t& cfg ){
	config_t xh = cfg;
	function_t f0 = constraintFunctionLocal(xh);
	numeric_t temp, epsilon=numeric_t(1.0e-8), h = epsilon;
	for (unsigned int j=0; j<N-6; ++j){
		xh = cfg;
		xh += h*tbasis.col(j);
		function_t f1 = constraintFunctionDH(xh);
		gradient_local.col(j) = (f1-f0)/h;
	}
	for (unsigned int j=0; j<6; ++j){
		xh = cfg;
		xh += h*obasis.col(j);
		function_t f1 = constraintFunctionDH(xh);
		gradient_local.col(N-6+j) = (f1-f0)/h;
	}
	return gradient_local;
}

template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::reduced_gradient_t lcbc<N,numeric_t>::gradientForwardDifferenceLocal( function_t& ftn ){
	config_t cfg = functionLocalToConfigLocal( ftn ), xh;
	reduced_gradient_t grad;
	function_t f0 = constraintFunction(cfg);
	numeric_t temp, epsilon=numeric_t(1.0e-8), h=epsilon;
	for (unsigned int j=0; j<6; ++j){
		xh = cfg;
		xh += h*obasis.col(j);
		function_t f1 = constraintFunctionDH(xh);
		grad.col(j) = (f1-f0)/h;
	}
	return grad;
}

/*** IMPLICIT FUNCTION THEOREM DERIVATIVE ***/
template < unsigned int N, typename numeric_t >
bool lcbc<N,numeric_t>::implicitFunctionTheoremDerivative( tuple<N-6,N>& angles ){
	return implicitFunctionTheorem(config_dh, angles);
}

template < unsigned int N, typename numeric_t >
bool lcbc<N,numeric_t>::implicitFunctionTheoremDerivative( config_t& cfg, tuple<N-6,N>& angles ){
	// update gradient
	gradientExactDH(cfg);

	// initialize usefull variables
	reduced_gradient_t Ax;
	Eigen::Matrix< numeric_t, 6, 1 > Ay, tmp;
	Eigen::FullPivHouseholderQR< reduced_gradient_t > qr;

	// compute reduced gradient
	for(unsigned int i = 0; i!= 6; ++i){
		Ax.col(i) = gradient_dh.col( angles.compl_data[i] );
	}
	qr.compute(Ax);
	// verify if reduced gradient is invertible
	if( not qr.isInvertible() ){
//		std::cout << Ax << std::endl << std::endl;
		return false;
	}
	
	// compute derivative using the implicit function theorem
	tbasis = tangent_basis_t::Zero();
	for(unsigned int j = 0; j<N-6; ++j){
		Ay = gradient_dh.col( angles.data[j] );
		tmp = qr.solve(-Ay);
		for(unsigned int i = 0; i!= 6; ++i){
			tbasis.col(j)( angles.compl_data[i] ) = tmp(i);
		}
		tbasis.col(j)( angles.data[j] ) = numeric_t(1.);
	}
	
	// basis need to be orthonormalized!	
	return true;
}

/*** COMPUTE BASIS ***/
template < unsigned int N, typename numeric_t >
bool lcbc<N,numeric_t>::computeLocalBasis(){
	return computeLocalBasis(config_dh);
}

template < unsigned int N, typename numeric_t >
bool lcbc<N,numeric_t>::computeLocalBasis( config_t& cfg ){
	tuple<N-6,N> angles;
	
	success = implicitFunctionTheoremDerivative( cfg, angles );
	while( success == false ){
		if( angles.update() == false ){
			success = false;
			return success;
		}
//		for( typename std::array<unsigned int, N >::iterator i = angles.data.begin(); i != angles.data.end(); ++i ){
//			std::cout << *i << " ";
//		}
//		std::cout << std::endl;
		success = implicitFunctionTheoremDerivative( cfg, angles );
	}
	
	Eigen::HouseholderQR< tangent_basis_t > qr;
	qr.compute(tbasis);
	complete_basis_t Q = qr.householderQ();
	tbasis = Q.leftCols(N-6);
	obasis = Q.rightCols(6);
	
	success = true;
	return success;
}

/*** UTILS ***/
template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::config_t lcbc<N,numeric_t>::functionLocalToConfigLocal( function_t& ftn ){
	config_t conf_local = config_t::Zero();
	for( unsigned int i = 0; i != N-6; ++i ){
		conf_local(i) = config_local(i);
	}
	for( unsigned int i = 0; i != 6; ++i ){
		conf_local(N-6+i) = ftn(i);
	}
	return conf_local;
}


template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::config_t lcbc<N,numeric_t>::configLocalToConfigDH( config_t& cfg ){
	config_t conf_dh = config_t::Zero();
	for( unsigned int i = 0; i != N-6; ++i ){
		conf_dh += cfg(i)*tbasis.col(i);
	}
	for( unsigned int i = 0; i != 6; ++i ){
		conf_dh += cfg(N-6+i)*obasis.col(i);
	}
	return config_dh + conf_dh;
}
		
template < unsigned int N, typename numeric_t >
typename lcbc<N,numeric_t>::config_t lcbc<N,numeric_t>::functionLocalToConfigDH( function_t& ftn ){
	config_t conf_local = functionLocalToConfigLocal( ftn );
	return configLocalToConfigDH( conf_local );
}

#endif

