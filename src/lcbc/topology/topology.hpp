#ifndef DH_TOPOLOGY_HPP
#define DH_TOPOLOGY_HPP

#include <map>
#include <biocpp/Standard>
#include "../dh_notation/dh_edge.hxx"

struct vertex{
	typedef BioCpp::standard::base::atom atom_t;
  atom_t* atom;
  void set( atom_t& atm ){
    atom = &atm;
  };
};

struct edge{
	unsigned int type;
  DH::edge<double> edge;
  void set(BioCpp::standard::base::atom& at1, BioCpp::standard::base::atom& at2){
  	DH::edge<double>::raw_edge_t p = std::make_pair(at1.coordinate, at2.coordinate);
  	edge.set(p);
  }
};


typedef BioCpp::topology<vertex, edge, boost::directedS> topology;

namespace all_atom{
	typedef BioCpp::standard::topology_constructor<vertex,edge,boost::directedS, BioCpp::atom::definition_t, BioCpp::component::definition_t> topology_constructor;
};


#endif
