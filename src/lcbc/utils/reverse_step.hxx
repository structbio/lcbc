#ifndef REVERSE_STEP_HXX
#define REFERSE_STEP_HXX

template < unsigned int N, typename numeric_t >
numeric_t reverse_step(typename lcbc<N,numeric_t>::config_t& config1, typename lcbc<N,numeric_t>::config_t& config2,typename lcbc<N,numeric_t>::tangent_basis_t& tangent){
  typename lcbc<N,numeric_t>::config_t ds = config2-config1;
  typename lcbc<N,numeric_t>::config_t proj; proj.setZero();
  for(unsigned int i = 0; i < tangent.cols(); ++i){
    proj += ds.dot( tangent.col(i) ) * tangent.col(i);
  }
  return proj.norm();
}

#endif
