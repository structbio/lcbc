#ifndef SET_CHAIN_CONFIG_HXX
#define SET_CHAIN_CONFIG_HXX

#include "../dh_notation/dh_chain.hxx"
#include "../dh_notation/dh_parameter_t.hxx"
#include <array>
#include <Eigen/Core>

template < unsigned int N, typename numeric_t >
void setChainConfig( DH::chain<N+1, numeric_t>& chain, typename std::array< typename DH::parameter_t<numeric_t>, N >& parameter_list, typename Eigen::Matrix< numeric_t, N, 1 >& conf_dh ){
  for(unsigned int j=0; j!=N; ++j){
		if(parameter_list[j].id == DH::parameter_id::D){
			chain.setD( conf_dh(j)/parameter_list[j].rescale, parameter_list[j].pos );
		}
		else if(parameter_list[j].id == DH::parameter_id::R){
			chain.setR( conf_dh(j)/parameter_list[j].rescale, parameter_list[j].pos );
		}
		else if(parameter_list[j].id == DH::parameter_id::ALPHA){
			chain.setAlpha( conf_dh(j)/parameter_list[j].rescale, parameter_list[j].pos );
		}
		else if(parameter_list[j].id == DH::parameter_id::THETA){
			chain.setTheta( conf_dh(j)/parameter_list[j].rescale, parameter_list[j].pos );
		}
		else if(parameter_list[j].id == DH::parameter_id::mCOS_ALPHA){
			chain.setAlpha( acos( -conf_dh(j)/parameter_list[j].rescale ), parameter_list[j].pos );
		}
		else if(parameter_list[j].id == DH::parameter_id::R3){
			chain.setR( pow( conf_dh(j), 1./3.)/parameter_list[j].rescale , parameter_list[j].pos );
		}
		else{
			std::cout << "Parameter " << parameter_list[j].id << "is not defined." << std::endl;
		}
	}
}

#endif
