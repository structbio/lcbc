#ifndef LOCAL_POSITION_LIST_T_HXX
#define LOCAL_POSITION_LIST_T_HXX

#include <Eigen/Core>
#include <map>
#include <list>

template <typename numeric_t>
class local_position_list_t{
  public:
    typedef typename Eigen::Matrix< numeric_t, 3, 1 > position_t;
    typedef typename Eigen::Matrix< numeric_t, 4, 1 > dh_position_t;
    typedef std::map< unsigned int, std::list<dh_position_t> > data_t;
  protected:
    unsigned int coord_num;
    dh_position_t positionToDhPosition( position_t& pos );
  public:  
    data_t data;
    void insert( unsigned int j, position_t pos );
    void insert( unsigned int j, dh_position_t pos );
    void insert( std::pair<unsigned int,position_t>& info );
    void insert( std::pair<unsigned int,dh_position_t>& info );
    void insert( std::list< std::pair<unsigned int,position_t> >& list );
    void insert( std::list< std::pair<unsigned int,dh_position_t> >& list );
    
    unsigned int size();
    
    local_position_list_t();
    local_position_list_t( std::list< std::pair<unsigned int,position_t> >& list );
};

template <typename numeric_t>
typename local_position_list_t<numeric_t>::dh_position_t local_position_list_t<numeric_t>::positionToDhPosition( typename local_position_list_t<numeric_t>::position_t& pos ){
  typename local_position_list_t<numeric_t>::dh_position_t dhpos;
  dhpos(0) = pos(0);
  dhpos(1) = pos(1);
  dhpos(2) = pos(2);
  dhpos(3) = numeric_t(1.);
  return dhpos; 
}

template <typename numeric_t>
void local_position_list_t<numeric_t>::insert( unsigned int j, position_t pos ){
  insert( j, positionToDhPosition(pos) );
} 

template <typename numeric_t>
void local_position_list_t<numeric_t>::insert( unsigned int j, dh_position_t pos ){
  data[j].push_back( pos );
  ++coord_num;
} 

template <typename numeric_t>
void local_position_list_t<numeric_t>::insert( std::pair<unsigned int,position_t>& info ){
  insert( info.first, info.second );
} 

template <typename numeric_t>
void local_position_list_t<numeric_t>::insert( std::pair<unsigned int,dh_position_t>& info ){
  insert( info.first, info.second );
} 

template <typename numeric_t>
void local_position_list_t<numeric_t>::insert( std::list< std::pair<unsigned int,position_t> >& list ){
  for( typename std::list< std::pair<unsigned int,position_t> >::iterator it = list.begin(); it!= list.end(); ++it ){
    insert( *it );
  }
} 

template <typename numeric_t>
void local_position_list_t<numeric_t>::insert( std::list< std::pair<unsigned int,dh_position_t> >& list ){
  for( typename std::list< std::pair<unsigned int,position_t> >::iterator it = list.begin(); it!= list.end(); ++it ){
    insert( *it );
  }
} 

template <typename numeric_t>
unsigned int local_position_list_t<numeric_t>::size(){
 return coord_num;
}

template <typename numeric_t>
local_position_list_t<numeric_t>::local_position_list_t(){
 coord_num = 0;
}

template <typename numeric_t>
local_position_list_t<numeric_t>::local_position_list_t( std::list< std::pair<unsigned int,position_t> >& list ){
 coord_num = 0;
 insert(list);
}


#endif
