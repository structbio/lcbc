#ifndef EDGES_TO_RAW_CHAIN
#define EDGES_TO_RAW_CHAIN

#include <boost/graph/adjacency_list.hpp>
#include "../dh_notation/dh_chain.hxx"

template <unsigned int N, typename numeric_t, typename topology_t>
class edges_to_raw_chain{
	protected:
		topology_t& topology;
	public:
		edges_to_raw_chain( topology_t& topo ): topology(topo){};
		
		typename DH::chain<N,numeric_t>::raw_chain_t operator()( typename std::array< typename topology_t::edge_iterator, N > edges ){
			typename DH::chain<N,numeric_t>::raw_chain_t raw_chain;
			for( unsigned int e = 0; e != N; ++e ){
				typename topology_t::vertex_t u = boost::source(*edges[e], topology.getGraph());
  			typename topology_t::vertex_t v = boost::target(*edges[e], topology.getGraph());
  			raw_chain[ e ] = { topology.getGraph()[u].atom->coordinate, topology.getGraph()[v].atom->coordinate };
			}
			return raw_chain;
		}
};

#endif
