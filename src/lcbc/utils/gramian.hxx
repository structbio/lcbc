#ifndef GRAMIAN_HXX
#define GRAMIAN_HXX

#include <array>
#include <Eigen/Core>
#include "../dh_notation/dh_chain.hxx"
#include "local_position_list_t.hxx"
#include "../dh_notation/dh_parameter_t.hxx"

template < unsigned int N, typename numeric_t >
numeric_t gramian( DH::chain<N+1, numeric_t>& chain, 
                   local_position_list_t<numeric_t>& pos, 
                   typename std::array< typename DH::parameter_t<numeric_t>, N >& par, 
                   typename Eigen::Matrix< numeric_t, N, N-6 >& tbasis 
                 ){
  typedef typename Eigen::Matrix<numeric_t,Eigen::Dynamic,N-6> M_matrix_t;
  typedef typename Eigen::Matrix<numeric_t,N-6,N-6> G_matrix_t;
  M_matrix_t M; M.setZero(3*pos.size(), N-6);
  unsigned int row = 0;
  for( typename local_position_list_t<numeric_t>::data_t::iterator d = pos.data.begin(); d != pos.data.end(); ++d ){
    std::map< unsigned int, typename Eigen::Matrix<numeric_t,4,4> > matrix_list;
    for( unsigned int p = 0; p != N; ++p ){
      matrix_list[p] = chain.matrixComputeFirstDerivative( par[p].id, par[p].pos, 0, d->first );
    }
    for( typename std::list< typename local_position_list_t<numeric_t>::dh_position_t >::iterator c = d->second.begin(); c != d->second.end(); ++c ){
      for( unsigned int p = 0; p != N; ++p ){
        for(unsigned int col = 0; col != N-6; ++col){
//std::cout << 28 << "  " << N << "  " << p << "  " << col << std::endl;		  
          Eigen::Matrix<numeric_t,4,1> vec4 = matrix_list[p]*(*c);
          Eigen::Matrix<numeric_t,3,1> vec3( vec4(0),vec4(1),vec4(2) );
         vec3*=tbasis(p,col);
//std::cout << vec3.transpose() << std::endl;
//std::cout << "###########" << std::endl;
					M(3*row,col) += vec3(0);		  
          M(3*row+1,col) += vec3(1);
          M(3*row+2,col) += vec3(2);
//std::cout << M << std::endl;
        }
      }
      ++row;
//std::cout << 41 << std::endl;		  
    }
  }
//std::cout << M << std::endl;
//std::cout << "########################################" << std::endl;		  
  G_matrix_t G = M.transpose()*M;
//std::cout << G << std::endl;
//std::cout << 46 << std::endl;		    
  return G.determinant();
} 

#endif
