#ifndef GET_CHAIN_CONFIG_HXX
#define GET_CHAIN_CONFIG_HXX

#include "../dh_notation/dh_chain.hxx"
#include "../dh_notation/dh_parameter_t.hxx"
#include <array>
#include <Eigen/Core>

template < unsigned int N, typename numeric_t >
typename Eigen::Matrix< numeric_t, N, 1 > getChainConfig( DH::chain<N+1, numeric_t>& chain, typename std::array< typename DH::parameter_t<numeric_t>, N >& parameter_list ){
  typename Eigen::Matrix< numeric_t, N, 1 > cfg;
	for(unsigned int j=0; j!=N; j++){
		if(parameter_list[j].id == DH::parameter_id::D){
			cfg(j) = chain.getD( parameter_list[j].pos )*parameter_list[j].rescale;
		}
		else if(parameter_list[j].id == DH::parameter_id::R){
			cfg(j) = chain.getR( parameter_list[j].pos )*parameter_list[j].rescale;
		}
		else if(parameter_list[j].id == DH::parameter_id::ALPHA){
			cfg(j) = chain.getAlpha( parameter_list[j].pos )*parameter_list[j].rescale;
		}
		else if(parameter_list[j].id == DH::parameter_id::THETA){
			cfg(j) = chain.getTheta( parameter_list[j].pos )*parameter_list[j].rescale;
		}
		else if(parameter_list[j].id == DH::parameter_id::mCOS_ALPHA){
			cfg(j) = -cos( chain.getAlpha( parameter_list[j].pos ) )*parameter_list[j].rescale;
		}
		else if(parameter_list[j].id == DH::parameter_id::R3){
			cfg(j) = pow( chain.getR( parameter_list[j].pos ), 3. )*parameter_list[j].rescale;
		}
		else{
			std::cout << "Parameter " << parameter_list[j].id << "is not defined." << std::endl;
		}
	}
	return cfg;
}

#endif
