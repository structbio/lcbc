#ifndef data_HPP
#define data_HPP

#include <array>
#include <set>
#include <iostream>

template< unsigned int K, unsigned int N >
class tuple{
	public:
		std::array<unsigned int, K >   data;
		std::array<unsigned int, N-K > compl_data;
		
		tuple();
		bool update();
};

template< unsigned int K, unsigned int N >
tuple<K,N>::tuple(){
	for(unsigned int k = 0; k != K; ++k){
		data[k] = k;
	}
	for(unsigned int k = K; k != N; ++k){
		compl_data[k-K] = k;
	}
}

template< unsigned int K, unsigned int N >
bool tuple<K,N>::update(){
	int k;
	
	std::set<unsigned int> all_idx;
	for(k = 0; k != N; ++k){
		all_idx.insert(k);
	}
	
	// update data
	bool stop = false, restart = false, failed = true;
	for( k = K-1; k != -1; --k){
		if( stop==false and data[k]==(N-1) ){
			data[k] = 0;
			restart = true;
		}
		else if( stop==false ){
			++data[k];
			all_idx.erase( data[k] );
			stop = true;
			failed = false;
		}
		else{
			all_idx.erase( data[k] );
			failed = false;
		}
	}
	if(failed){
		return false;
	}
	if(restart){
		for( k = 1; k!=K; ++k){
			if(data[k] == 0){
				data[k] = data[k-1]+1;
				all_idx.erase( data[k] );
			}
			if(data[k]>(N-1)){
				return false;
			}
		}
	}
	// update compl_data
	k = 0;
	for(std::set<unsigned int>::iterator idx = all_idx.begin(); idx!=all_idx.end(); ++idx,++k){
		compl_data[k]= (*idx);
	}
	return true;
}

#endif
