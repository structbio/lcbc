#ifndef DISTANCE_HXX
#define DISTANCE_HXX
#include "../lcbc.hxx"

template < unsigned int N, typename numeric_t >
numeric_t distance( typename lcbc<N,numeric_t>::config_t& config1, typename lcbc<N,numeric_t>::config_t& config2, typename lcbc<N,numeric_t>::parameter_list_t& params ){
	numeric_t dist = 0.;
	for(unsigned int j = 0; j < config1.size(); j++){
    if( params[j].id == DH::parameter_id::THETA or params[j].id == DH::parameter_id::ALPHA  ){
    	numeric_t length = fabs( config1[j]-config2[j] );
			numeric_t toulen = fmod(length,2*M_PI*params[j].rescale);
   	 	numeric_t proj = std::min( toulen, M_PI*params[j].rescale-toulen );
   	 	dist += proj*proj;
    }
    else{
	    numeric_t proj = config1[j] - config2[j];
			dist += proj*proj;
		}
  }
  return sqrt(dist);
}
#endif
