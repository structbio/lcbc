#ifndef CONFIG_VISITOR
#define CONFIG_VISITOR

#include "../lcbc.hxx"

template <unsigned int N, typename numeric_t>
class config_visitor{
	public:
		virtual void onDirectionIsOrthogonalToTangentSpace( typename lcbc<N,numeric_t>::config_t& config_dh,
		                                                    typename lcbc<N,numeric_t>::tangent_basis_t& tbasis, 
		                                            				typename lcbc<N,numeric_t>::orthogonal_basis_t& obasis,
		                                            				typename lcbc<N,numeric_t>::config_t& direction, 
		                                            				typename lcbc<N,numeric_t>::config_t& projected_dir_local 
		                                          				){};
		virtual void onRootFindingSuccess( typename lcbc<N,numeric_t>::config_t& old_config,
						                           typename lcbc<N,numeric_t>::tangent_basis_t& old_tbasis, 
		                          	       typename lcbc<N,numeric_t>::orthogonal_basis_t& old_obasis, 
		                                   typename lcbc<N,numeric_t>::config_t& config_dh,
						                           typename lcbc<N,numeric_t>::tangent_basis_t& tbasis,
                                       typename lcbc<N,numeric_t>::orthogonal_basis_t& obasis
		                         	       ){};
		virtual void onRootFindingConvergenceFault( typename lcbc<N,numeric_t>::config_t& config_dh,
		                                            typename lcbc<N,numeric_t>::tangent_basis_t& tbasis, 
		                                            typename lcbc<N,numeric_t>::orthogonal_basis_t& obasis,
		                                            typename lcbc<N,numeric_t>::config_t& projected_dir_local 
		                         				 					){};
		virtual void onImplicitFunctionTheoremGradientIsNotInvertible( typename lcbc<N,numeric_t>::config_t old_config,
						                                                       typename lcbc<N,numeric_t>::tangent_basis_t old_tbasis, 
		                          	                                   typename lcbc<N,numeric_t>::orthogonal_basis_t old_obasis,
		                          	                                   typename lcbc<N,numeric_t>::config_t config_dh,
		                                                               typename lcbc<N,numeric_t>::tangent_basis_t tbasis, 
		                           				 					                   typename lcbc<N,numeric_t>::orthogonal_basis_t obasis
		                           				 					                 ){};
};

#endif
