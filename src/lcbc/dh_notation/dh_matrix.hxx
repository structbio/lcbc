#ifndef DENAVIT_HARTENBERG_NOTATION_MATRIX
#define DENAVIT_HARTENBERG_NOTATION_MATRIX

#include <Eigen/Core>
#include "dh_parameter_t.hxx"
namespace DH{

template <typename numeric_t>
class matrix{
	public:
		typedef typename Eigen::Matrix<numeric_t, 4, 4> matrix_t;
		
		matrix_t compute( numeric_t& alpha, numeric_t& theta, numeric_t& r, numeric_t& d );
		matrix_t computeFirstDerivative( numeric_t& alpha, numeric_t& theta, numeric_t& r, numeric_t& d, unsigned int par_type );
};

template <typename numeric_t>
typename matrix<numeric_t>::matrix_t matrix<numeric_t>::compute( numeric_t& alpha, numeric_t& theta, numeric_t& r, numeric_t& d ){
	numeric_t sa = sin(alpha);
	numeric_t ca = cos(alpha);
	numeric_t st = sin(theta);
	numeric_t ct = cos(theta);
	matrix_t result;
	result << ct , -st*ca , st*sa  , d*ct ,
	          st , ct*ca  , -ct*sa , d*st , 
	          0.  , sa     , ca     , r   ,
	          0. , 0.     , 0.     , 1.   ;
	return result;
}

template <typename numeric_t>
typename matrix<numeric_t>::matrix_t matrix<numeric_t>::computeFirstDerivative( numeric_t& alpha, numeric_t& theta, numeric_t& r, numeric_t& d, unsigned int par_type ){
	matrix_t result;
	numeric_t st = sin(theta);
	numeric_t ct = cos(theta);
	numeric_t sa = sin(alpha);
	numeric_t ca = cos(alpha);
	if(par_type == parameter_id::D){
		result << 0. , 0. , 0. , ct ,
		          0. , 0. , 0. , st , 
		          0. , 0. , 0. , 0. ,
		          0. , 0. , 0. , 0. ;
		return result;
	}
	if(par_type == parameter_id::R){
		result << 0. , 0. , 0. , 0 ,
		          0. , 0. , 0. , 0 , 
		          0. , 0. , 0. , 1. ,
		          0. , 0. , 0. , 0. ;
		return result;
	}
	if(par_type == parameter_id::ALPHA){
			result << 0. , st*sa , st*ca  , 0. ,
	          0. , -ct*sa  , -ct*ca , 0. , 
	          0.  , ca     , -sa     , 0.   ,
	          0. , 0.     , 0.     , 0.   ;
		return result;
	}
	if(par_type == parameter_id::THETA){
		result << -st , -ct*ca , ct*sa  , -d*st ,
	          ct , -st*ca  , st*sa , d*ct , 
	          0.  , 0.     , 0.     , 0.   ,
	          0. , 0.     , 0.     , 0.   ;
		return result;
	}
	if(par_type == parameter_id::mCOS_ALPHA){
		numeric_t cota = tan(M_PI/2. - alpha);
		result << 0. , st , st*cota  , 0. ,
	          0. , -ct  , -ct*cota , 0. , 
	          0.  , cota     , -1.     , 0.   ,
	          0. , 0.     , 0.     , 0.   ;
		return result;
	}
	if(par_type == parameter_id::R3){
		double fact = 1./(3.*r*r);
		result << 0. , 0. , 0.  , 0. ,
	          0. , 0.  , 0. , 0. , 
	          0.  , 0.     , 0.     , fact   ,
	          0. , 0.     , 0.     , 0.   ;
		return result;
	}
	return result;
}

} // end namespace DH

#endif
