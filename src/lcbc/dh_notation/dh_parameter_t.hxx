#ifndef DENAVIT_HARTENBERG_NOTATION_PARAMETER_TYPE
#define DENAVIT_HARTENBERG_NOTATION_PARAMETER_TYPE

#include <array>

namespace DH{

	enum parameter_id: unsigned int { ALPHA=0,THETA=1,R=2,D=3,mCOS_ALPHA=4,R3=5 };

	template <typename numeric_t>
	struct parameter_t{
		parameter_id id;
		unsigned int pos;
		numeric_t rescale;
		
		parameter_t(){};
		parameter_t( parameter_id i, unsigned int p, numeric_t r ): id(i), pos(p), rescale(r) {};
		parameter_t( std::array<numeric_t,3> arr ) : id(arr[0]), pos(arr[1]),rescale(arr[2]) {};
	};
	
} //end namespace DH

#endif
