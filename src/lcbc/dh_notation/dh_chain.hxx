#ifndef DENAVIT_HARTENBERG_NOTATION_CHAIN
#define DENAVIT_HARTENBERG_NOTATION_CHAIN

#include <array>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <iostream>
#include "dh_edge.hxx"
#include "dh_matrix.hxx"

namespace DH{

template <unsigned int N, typename numeric_t>
class chain{
	public:
		typedef typename Eigen::Matrix<numeric_t, N-1, 1> config_t;
  	typedef typename matrix<numeric_t>::matrix_t matrix_t;
  	typedef typename edge<numeric_t>::vector_t vector_t;
  	typedef typename edge<numeric_t>::raw_edge_t raw_edge_t;
  	typedef typename std::array<raw_edge_t,N > raw_chain_t;
  protected:
  	config_t v_alpha;
  	config_t v_theta;
  	config_t v_r;
  	config_t v_d;
  	typename std::array< edge<numeric_t>, N > reference_frame;
  private:
  	numeric_t initAlpha( edge<numeric_t>& current_edge, edge<numeric_t>& previous_edge );
  	numeric_t initTheta( edge<numeric_t>& current_edge, edge<numeric_t>& previous_edge );
  	numeric_t initR( edge<numeric_t>& current_edge, edge<numeric_t>& previous_edge );
  	numeric_t initD( edge<numeric_t>& current_edge, edge<numeric_t>& previous_edge );
  public:
  	chain(){};
  	chain( raw_chain_t& chain );
  	
  	void setAlpha( config_t& alpha );
  	void setAlpha( numeric_t alpha, unsigned int j );
  	void setTheta( config_t& theta );
  	void setTheta( numeric_t theta, unsigned int j );
  	void setR( config_t& r );
  	void setR( numeric_t r, unsigned int j );
  	void setD( config_t& d );
  	void setD( numeric_t d, unsigned int j );
  	
  	config_t getAlpha();
  	numeric_t getAlpha(unsigned int j);
  	config_t getTheta();
  	numeric_t getTheta(unsigned int j);
  	config_t getR();
  	numeric_t getR(unsigned int j);
  	config_t getD();
  	numeric_t getD(unsigned int j);
  	
  	typename std::array< edge<numeric_t>, N > getReferenceFrame();
  	edge<numeric_t> getReferenceFrame(unsigned int j);
  	
  	
  	matrix_t matrixCompute(unsigned int f, unsigned int l);
  	matrix_t matrixCompute( config_t& a, config_t& t, config_t& r, config_t& d, unsigned int f, unsigned int l );
  	matrix_t matrixComputeFirstDerivative( unsigned int par_type, unsigned int k, unsigned int f, unsigned int l );
  	
  	matrix_t matrixCompute();
  	matrix_t matrixCompute( config_t& a, config_t& t, config_t& r, config_t& d );
  	matrix_t matrixComputeFirstDerivative( unsigned int par_type, unsigned int k );
};

// constructor
template <unsigned int N, typename numeric_t>
chain<N,numeric_t>::chain( raw_chain_t& chain ){
	// initialize a list of edges
	for( unsigned int e = 0; e != N; ++e ){
		reference_frame[e].set( chain[e] );
	}
	// set the reference frame of the first edge
	reference_frame[0].origin = reference_frame[0].source;
	reference_frame[0].xdir = reference_frame[0].zdir.cross( reference_frame[1].zdir );
	reference_frame[0].xdir.normalize();
	reference_frame[0].ydir = reference_frame[0].zdir.cross( reference_frame[0].xdir );
	reference_frame[0].ydir.normalize();
	// set the reference frame of all the other edges
	for( unsigned int e = 1; e != N; ++e ){
		reference_frame[e].computeFrame( reference_frame[e-1] );
	}
	// compute DH parameters
	for( unsigned int e = 1; e != N; ++e ){
		v_alpha[e-1] = initAlpha( reference_frame[e], reference_frame[e-1] );
		v_theta[e-1] = initTheta( reference_frame[e], reference_frame[e-1] );
		v_r[e-1] = initR( reference_frame[e], reference_frame[e-1] );
		v_d[e-1] = initD( reference_frame[e], reference_frame[e-1] );
	}
}

// initialize alpha
template <unsigned int N, typename numeric_t>
numeric_t chain<N,numeric_t>::initAlpha( edge<numeric_t>& current_edge, edge<numeric_t>& previous_edge ){
	vector_t crossv = ( previous_edge.zdir ).cross( current_edge.zdir );
	numeric_t alpha =  asin( crossv.norm() );
	if( previous_edge.zdir.dot(current_edge.zdir) < 0. ){
		alpha = M_PI-alpha;
	}
	if(crossv.dot(current_edge.xdir) > 0.){
		return alpha;
	}
	return -alpha;
}

//initialize theta
template <unsigned int N, typename numeric_t>
numeric_t chain<N,numeric_t>::initTheta( edge<numeric_t>& current_edge, edge<numeric_t>& previous_edge ){
	vector_t cross = (previous_edge.xdir).cross(current_edge.xdir);
	numeric_t theta = asin( cross.norm() );
	if(previous_edge.xdir.dot(current_edge.xdir) < 0.){
		theta = M_PI-theta;
	}
	if(cross.dot(previous_edge.zdir) > 0.){
		return theta;
	}
	return -theta;

}

// initialize d
template <unsigned int N, typename numeric_t>
numeric_t chain<N,numeric_t>::initD( edge<numeric_t>& current_edge, edge<numeric_t>& previous_edge ){
	return (current_edge.origin - previous_edge.origin).dot( current_edge.xdir );
}

// initialize r
template <unsigned int N, typename numeric_t>
numeric_t chain<N,numeric_t>::initR( edge<numeric_t>& current_edge, edge<numeric_t>& previous_edge ){
	return (current_edge.origin - previous_edge.origin).dot( previous_edge.zdir );
}

// set alpha
template <unsigned int N, typename numeric_t>
void chain<N,numeric_t>::setAlpha( config_t& alpha ){
	v_alpha = alpha;
}

template <unsigned int N, typename numeric_t>
void chain<N,numeric_t>::setAlpha( numeric_t alpha, unsigned int j ){
	v_alpha[j] = alpha;
}

// set theta
template <unsigned int N, typename numeric_t>
void chain<N,numeric_t>::setTheta( config_t& theta ){
	v_theta = theta;
}

template <unsigned int N, typename numeric_t>
void chain<N,numeric_t>::setTheta( numeric_t theta, unsigned int j ){
	v_theta[j] = theta;
}

// set r
template <unsigned int N, typename numeric_t>
void chain<N,numeric_t>::setR( config_t& r ){
	v_r = r;
}

template <unsigned int N, typename numeric_t>
void chain<N,numeric_t>::setR( numeric_t r, unsigned int j ){
	v_r[j] = r;
}

// set d
template <unsigned int N, typename numeric_t>
void chain<N,numeric_t>::setD( config_t& d ){
	v_d = d;
}

template <unsigned int N, typename numeric_t>
void chain<N,numeric_t>::setD( numeric_t d, unsigned int j ){
	v_d[j] = d;
}

// get alpha
template <unsigned int N, typename numeric_t>  	
typename chain<N,numeric_t>::config_t chain<N,numeric_t>::getAlpha(){
	return v_alpha;
}

template <unsigned int N, typename numeric_t>  	
numeric_t chain<N,numeric_t>::getAlpha(unsigned int j){
	return v_alpha[j];
}

// get theta
template <unsigned int N, typename numeric_t>
typename chain<N,numeric_t>::config_t chain<N,numeric_t>::getTheta(){
	return v_theta;
}

template <unsigned int N, typename numeric_t>  	
numeric_t chain<N,numeric_t>::getTheta(unsigned int j){
	return v_theta[j];
}

// get r
template <unsigned int N, typename numeric_t>
typename chain<N,numeric_t>::config_t chain<N,numeric_t>::getR(){
	return v_r;
}

template <unsigned int N, typename numeric_t>  	
numeric_t chain<N,numeric_t>::getR(unsigned int j){
	return v_r[j];
}

// get d
template <unsigned int N, typename numeric_t>
typename chain<N,numeric_t>::config_t chain<N,numeric_t>::getD(){
	return v_d;
}

template <unsigned int N, typename numeric_t>  	
numeric_t chain<N,numeric_t>::getD(unsigned int j){
	return v_d[j];
}

// compute DH matrix
template <unsigned int N, typename numeric_t>
typename chain<N,numeric_t>::matrix_t chain<N,numeric_t>::matrixCompute(unsigned int f, unsigned int l){
	return matrixCompute( v_alpha,v_theta,v_r,v_d, f, l );
}

// compute DH matrix
template <unsigned int N, typename numeric_t>
typename chain<N,numeric_t>::matrix_t chain<N,numeric_t>::matrixCompute( config_t& a, config_t& t, config_t& r, config_t& d, unsigned int f, unsigned int l ){
	if( l<f ){
	  std::cout << "Last index must be greater than first one." << std::endl;
		exit(1);
	}

	matrix<numeric_t> dhmtx;
	typename chain<N,numeric_t>::matrix_t result;
	result.setIdentity();
	
	unsigned int last = std::min( N-1, l+1 );
	for(unsigned i = f; i < last; ++i){
		result*=dhmtx.compute( a(i), t(i), r(i), d(i) );
	}
	return result;
}

// compute first derivative of DH matrix
template <unsigned int N, typename numeric_t>
typename chain<N,numeric_t>::matrix_t chain<N,numeric_t>::matrixComputeFirstDerivative( unsigned int par_type, unsigned int k, unsigned int f, unsigned int l ){
	if( k > N - 1){
		std::cout << "Index " << k << " is too large. Cannot derive." << std::endl;
		exit(1);
	}
	else if( l<f ){
	  std::cout << "Last index must be greater than first one." << std::endl;
		exit(1);
	}
	matrix<numeric_t> dhmtx;
	typename chain<N,numeric_t>::matrix_t result;
	result.setZero();

	unsigned int last = std::min( N-1, l+1 );
	if(k<f or k>=last){
	  return result;
	}

	result.setIdentity();
	for(unsigned i = f; i < last; ++i){
		if(i==k){
			result*=dhmtx.computeFirstDerivative( v_alpha(i),v_theta(i),v_r(i),v_d(i), par_type );
		}
		else{
			result*=dhmtx.compute( v_alpha(i),v_theta(i),v_r(i),v_d(i) );
		}
	}
	return result;
}

// compute DH matrix
template <unsigned int N, typename numeric_t>
typename chain<N,numeric_t>::matrix_t chain<N,numeric_t>::matrixCompute(){
	return matrixCompute( v_alpha,v_theta,v_r,v_d,0,N-1 );
}

// compute DH matrix
template <unsigned int N, typename numeric_t>
typename chain<N,numeric_t>::matrix_t chain<N,numeric_t>::matrixCompute( config_t& a, config_t& t, config_t& r, config_t& d ){
	return matrixCompute(a,t,r,d,0,N-1);
}

// compute first derivative of DH matrix
template <unsigned int N, typename numeric_t>
typename chain<N,numeric_t>::matrix_t chain<N,numeric_t>::matrixComputeFirstDerivative( unsigned int par_type, unsigned int k ){
	return matrixComputeFirstDerivative(par_type,k,0,N-1);
}

template <unsigned int N, typename numeric_t>
typename std::array< edge<numeric_t>, N > chain<N,numeric_t>::getReferenceFrame(){
  return reference_frame;
}

template <unsigned int N, typename numeric_t>
edge<numeric_t> chain<N,numeric_t>::getReferenceFrame(unsigned int j){
  return reference_frame[j];
}

} // end namespace DH
#endif
