#ifndef DENAVIT_HARTENBERG_NOTATION_EDGE
#define DENAVIT_HARTENBERG_NOTATION_EDGE

#include <Eigen/Core>

namespace DH{

template <unsigned int N, typename scalar_t>
class chain;

template <typename numeric_t>
class edge{
	public:
		typedef typename Eigen::Matrix<numeric_t,3,1> vector_t;
		typedef typename std::pair< vector_t, vector_t > raw_edge_t;
  protected:
    vector_t source; // coordinate of the first point in the edge
		vector_t target; // coordinate of the last point in the edge
		vector_t origin; // reference frame
		vector_t zdir;   // z axis
		vector_t ydir;   // y axis
		vector_t xdir;   // x axis
  public:
    edge(){};
    edge( raw_edge_t& e );
    
    void set( raw_edge_t& e );
    
    void computeFrame(edge<numeric_t>& previous_edge );                 /* compute new frame origin and xdir. */
	
		bool isParallelTo( edge<numeric_t>& e );                                                   /* return true if 'e' is parallel to *this */
		bool doesIntersect( edge<numeric_t>& e, vector_t& intersection, numeric_t eps = 10e-7 );   /* return true if 'e' intersects *this. 'intersection' will contain the intersection point */
		
		template< unsigned int N, typename scalar_t >
		friend class chain;
};

template <typename numeric_t>
edge<numeric_t>::edge( raw_edge_t& e ){
	set(e);
}

template <typename numeric_t>
void edge<numeric_t>::set( raw_edge_t& e ){
	source = e.first;
	target = e.second;
	zdir = target-source;
	zdir.normalize();
}

template <typename numeric_t>
void edge<numeric_t>::computeFrame(edge<numeric_t>& previous_edge ){
	if( isParallelTo( previous_edge ) ){
		origin=source;
		vector_t diff = source - previous_edge.source;
		vector_t par = ( diff.dot( previous_edge.zdir ) )*previous_edge.zdir;
		xdir = diff-par;
		xdir.normalize();
		ydir = zdir.cross(xdir);
		ydir.normalize();
		return;
	}
	if( doesIntersect( previous_edge, origin ) ){
		xdir = previous_edge.zdir.cross(zdir);
		xdir.normalize();
		ydir = zdir.cross(xdir);
		ydir.normalize();
		return;
	}
	vector_t diff = previous_edge.source-source;
	numeric_t vec1_diff = diff.dot( previous_edge.zdir );
	numeric_t vec2_diff = diff.dot( zdir );
	numeric_t vec1_vec2 = previous_edge.zdir.dot( zdir );
	numeric_t mu1 = ( vec2_diff * vec1_vec2 - vec1_diff ) / 
 	         		    ( 1. - vec1_vec2 * vec1_vec2 );
	numeric_t mu2 = ( vec2_diff + vec1_vec2 * mu1);
	origin = source + mu2*zdir;
	vector_t pr_intersect = previous_edge.source + mu1*previous_edge.zdir;
	xdir = origin - pr_intersect;
	xdir.normalize();
	ydir = zdir.cross(xdir);
	ydir.normalize();
}

template <typename numeric_t>
bool edge<numeric_t>::isParallelTo(edge<numeric_t>& e ){
	if( e.zdir == zdir or e.zdir == -zdir )
		return true;
	return false;
}

template <typename numeric_t>
bool edge<numeric_t>::doesIntersect(edge<numeric_t>& e, vector_t& intersection, numeric_t eps ){
	if(source == e.target){
		intersection = source;
		return true;
	}
	if(target == e.source){
		intersection = target;
		return true;
	}
	
	vector_t crossv = zdir.cross( e.zdir );
	crossv.normalize();
	numeric_t distance = crossv.dot( source - e.source );

	if( fabs( distance ) < eps ){
		vector_t R = e.source - source;
		numeric_t Rb = R.dot( e.zdir );
		numeric_t Ra = R.dot( zdir );
		numeric_t ab = e.zdir.dot( zdir );
		numeric_t mu = ( Rb - Ra*ab )/( ab*ab - 1. );
		intersection = e.source + mu*e.zdir;
		return true; 
	}
	return false;
}

} // end namespace DH

#endif
