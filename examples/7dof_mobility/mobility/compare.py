subroutine plotMobility( xmin, xmax, ymin, ymax, pdbId, shift, rescaleFactor, panel_label ){
	set numeric display latex
	set unit angle nodimensionless
	set key top xcenter
	set size ratio 1.
	set xlabel "residue id"
	set ylabel r"mobility  ($\AA^2$)"
	set xrange[xmin:xmax]
	set yrange[ymin:ymax]
	set xlabel ""
	set no xtics
	set axis x mirrored
	set axis y visible
	set title "(%s)"%(panel_label)
	exec 'plot "%s/var_%s.dat" using 0:2 w l title "experimental", "%s/mob_%s.dat" using ($0+1):($2/%f) w l title "theoretical"'%(pdbId,pdbId,pdbId,pdbId,rescaleFactor)

}

subroutine plotSecondaryStructure(xmin, xmax, ymin, ymax, pdbId, shift, xorigin){
	set origin xorigin,-0.7
	set xrange[xmin:xmax]
	set yrange[ymin:1]
	set ylabel ""
	set xtics
	set notitle
	set size ratio 0.04
	set axis x nomirrored
	set axis y invisible
	set xlabel "residue id"
	plot "%s/beta_%s.dat"%(pdbId,pdbId) using (($1+$2)/2.):(1):(($2-$1)) with wboxes fillcolor yellow title "", "%s/alpha_%s.dat"%(pdbId,pdbId) using (($1+$2)/2.):(1):(($2-$1)) with wboxes fillcolor red title ""

}

subroutine plotAllFiles( xmin, xmax, ymin, ymax, pdbId, shift, rescaleFactor, xorigin, panel_label ){
	set multiplot
	set xrange[xmin:xmax]
	set yrange[ymin:ymax]
	
	call plotMobility(xmin, xmax, ymin, ymax, pdbId, shift, rescaleFactor, panel_label);
	call plotSecondaryStructure(xmin, xmax, ymin, ymax, pdbId, shift, xorigin);
}

set preamble r"\usepackage{helvet}"
set preamble r"\renewcommand{\familydefault}{\sfdefault}"

set terminal eps color solid antialias dpi 450 
set output "output.eps"

set multiplot
#call plotAllFiles(0,150,0,10,"1WNJ",0,40.)
call plotAllFiles(0,120,0,25,"1YGM",0,5.,0., 'A')
set origin 10,0
call plotAllFiles(0,165,0,8,"2ITH",0,30.,10., 'B')
unset multiplot
set origin 0,0
#############################################################

set terminal tiff color solid antialias dpi 450
set output "output.tiff"

set multiplot
#call plotAllFiles(0,150,0,10,"1WNJ",0,40.)
call plotAllFiles(0,120,0,25,"1YGM",0,5.,0., 'A')
set origin 10,0
call plotAllFiles(0,165,0,8,"2ITH",0,30.,10., 'B')
unset multiplot
set origin 0,0
