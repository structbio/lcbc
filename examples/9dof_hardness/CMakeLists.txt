set( Boost_LIBRARIES ${Boost_LIBRARIES} PARENT_SCOPE )
set( CGAL_LIBRARIES ${CGAL_LIBRARIES} PARENT_SCOPE )

project( 9dof_hardness )

add_executable(${PROJECT_NAME} ${PROJECT_NAME}.cpp)

target_link_libraries(${PROJECT_NAME} ${Boost_LIBRARIES} ${CGAL_LIBRARIES} biocpp-standard)
