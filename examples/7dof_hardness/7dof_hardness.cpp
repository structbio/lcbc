#include <map>
#include <array>
#include <algorithm>
#include <vector>

#include "../../src/lcbc/lcbc.hxx"
#include "../../src/lcbc/utils/config_visitor.hxx"
#include "../../src/lcbc/utils/edges_to_raw_chain.hxx"
#include "../../src/lcbc/utils/distance.hxx"
#include "../../src/lcbc/utils/reverse_step.hxx"
#include "../../src/lcbc/topology/topology.hpp"

#include <biocpp/Core>
#include <biocpp/algorithm/geometric/pivot.hxx>

typedef BioCpp::Core::atom atom;
typedef BioCpp::io::model<atom>::type model;
typedef BioCpp::element::dictionary_t element_dictionary;
typedef BioCpp::atom::dictionary_t    atom_dictionary;
typedef BioCpp::component::dictionary_t residue_dictionary;
typedef BioCpp::io::pdb::print_atom_t< element_dictionary, atom_dictionary, residue_dictionary > print_atom_t;

const int N = 8; // this has to be equal to eight! do not change!

// config visitor: it computes manifold length
template <unsigned int N, typename numeric_t>
class manifold7dof_visitor : public config_visitor<N,numeric_t>{
		typedef typename lcbc<N,numeric_t>::config_t config_t;
	public:
		config_t proj_dir;
		config_t first_dir;
		
		unsigned int mdl;
		
		model& mod;
		topology& topo;
		std::array< topology::edge_iterator,N>& edges;
		bool go_on;
		bool can_stop;
		
		element_dictionary eleDict;
		atom_dictionary atmDict;
		residue_dictionary resDict;
		
		typename lcbc<N,numeric_t>::config_t start_config;
		typename lcbc<N,numeric_t>::parameter_list_t params;
		numeric_t step_length;
		
		manifold7dof_visitor( model& m,
		                      topology& t, 
		                      std::array< topology::edge_iterator,N>& e,
		                      numeric_t ds,
		                      typename lcbc<N,numeric_t>::parameter_list_t p,
		                      element_dictionary& ed,
		                      atom_dictionary& ad,
		                      residue_dictionary& rd
		                    ): mod(m), topo(t), edges(e), step_length(ds), params(p), eleDict(ed), atmDict(ad), resDict(rd) {
		
			go_on = true;
			can_stop = false;
			mdl = 0;
			start_config = lcbc<N,numeric_t>::config_t::Zero();
			first_dir = lcbc<N,numeric_t>::config_t::Zero();
		};
		
		void onRootFindingConvergenceFault( typename lcbc<N,numeric_t>::config_t& config_dh,
		                                    typename lcbc<N,numeric_t>::tangent_basis_t& tbasis, 
		                                    typename lcbc<N,numeric_t>::orthogonal_basis_t& obasis,
		                                    typename lcbc<N,numeric_t>::config_t& projected_dir_local 
		                         				 	){
		  // if algorithm failed to converge, try to come back
		  proj_dir = config_t::Zero();
			for( unsigned int i = 0; i != N-6; ++i ){
				proj_dir -= projected_dir_local(i)*tbasis.col(i);
			}
			std::cout << "Cannot converge to solution" << std::endl;
		};

		void onImplicitFunctionTheoremGradientIsNotInvertible( typename lcbc<N,numeric_t>::config_t old_config,
						                                               typename lcbc<N,numeric_t>::tangent_basis_t old_tbasis, 
		                          	                           typename lcbc<N,numeric_t>::orthogonal_basis_t old_obasis,
		                          	                           typename lcbc<N,numeric_t>::config_t config_dh,
		                                                       typename lcbc<N,numeric_t>::tangent_basis_t tbasis, 
		                           				 					           typename lcbc<N,numeric_t>::orthogonal_basis_t obasis
		                           				 					         ){
			std::cout << "Cannot apply implicit function theorem" << std::endl;
		}

		void onRootFindingSuccess( typename lcbc<N,numeric_t>::config_t& old_config,
						                   typename lcbc<N,numeric_t>::tangent_basis_t& old_tbasis, 
		                           typename lcbc<N,numeric_t>::orthogonal_basis_t& old_obasis, 
		                           typename lcbc<N,numeric_t>::config_t& config_dh,
						                   typename lcbc<N,numeric_t>::tangent_basis_t& tbasis,
                               typename lcbc<N,numeric_t>::orthogonal_basis_t& obasis
		                         ){
		  // update direction
		  proj_dir = config_dh - old_config;
			
			// save initial data
			if( start_config == lcbc<N,numeric_t>::config_t::Zero() ){
				start_config = config_dh;
			}
			if( first_dir == lcbc<N,numeric_t>::config_t::Zero() and start_config != lcbc<N,numeric_t>::config_t::Zero() ){
				first_dir = proj_dir;
			}
			
			// decide if the manifold has been completely explored
			numeric_t dist = distance<N,numeric_t>( config_dh, start_config, params );
			if( not can_stop and  dist > 2*step_length ){
				can_stop = true;
			}
			else if( can_stop and dist < 2*step_length and first_dir.dot(proj_dir)>0. ){
				go_on = false;
			}
      
      for(unsigned int i = 0; i < config_dh.rows(); ++i){
	  		std::cout << (config_dh(i)-old_config(i)) << "  ";
	  	}
	  	std::cout << std::endl;
		};
};

// usage ./backbone_mobility pdb_filename chain_id
int main(int argc, char* argv[]){

  // Parse input parameters
	unsigned int input_flag = 0;
	bool usage_flag = false;
	const char* dic_filename;
  const char* pdb_filename;
	char chain_id;
	unsigned int first_res;
	unsigned int bond;
	unsigned int hard_dof = 0;
	double hardness = 10.;
	
	int c;
  while ((c = getopt (argc, argv, "d:p:c:r:b:H:l:h")) != -1){
    switch (c){
      case 'd':
        input_flag+=1;
        dic_filename = optarg;
        break;
      case 'p':
        input_flag+=2;
        pdb_filename = optarg;
        break;
      case 'c':
        input_flag+=4;
        chain_id = optarg[0];
        break;
      case 'r':
        input_flag+=8;
        first_res = atoi(optarg);
        break;
      case 'b':
        input_flag+=16;
        bond = atoi(optarg);
        break;
      case 'H':
        input_flag+=32;
        hard_dof = atoi(optarg);
        break;
      case 'l':
        input_flag+=64;
        hardness = atof(optarg);
        break;
      case 'h':
        usage_flag = true;
        break;
    }
  }
	if( input_flag != 127 ){
		std::cout << input_flag << std::endl;
	  usage_flag = true;
	}
  
  // print an help message if necessary
  if( usage_flag ){
    std::cout << "usage: .7dof_explore -d dictFile (string) -p pdbFile (string) -c chainId (char) -r firstRes (int) -b bond (int) -H hard_dof (unsigned int) -l hardness (double)" << std::endl
              << "Description: " << std::endl
              << "\t-d: dictionary filename" << std::endl
              << "\t-p: pdb filename" << std::endl
              << "\t-c: chain id" << std::endl
              << "\t-r: first residue composing the fragment" << std::endl
              << "\t-H: index of the Hard degree of freedom" << std::endl
              << "\t-l: hardness" << std::endl
              << "\t-h: help" << std::endl
              << "!!! All input parameters are required !!!" << std::endl;
    return 1;
  }
  
	unsigned int last_res = first_res+3;
	
  // reading dictionaries
  libconfig::Config cfg;
  cfg.readFile(dic_filename);
  libconfig::Setting& root = cfg.getRoot();
  element_dictionary eleDict;
  eleDict.importSetting(root,{"elements"});
  atom_dictionary atmDict;
  atmDict.importSetting(root,{"atoms"});
  residue_dictionary resDict;
  resDict.importSetting(root,{"components"});
  BioCpp::io::pdb::file PDB(pdb_filename, eleDict, atmDict, resDict, 0); // read the pdb file. 
	model mdl = PDB.readModel<atom>(1, eleDict, atmDict, resDict);
	
 	// Build topology
  all_atom::topology_constructor topo_constr;
  topology topo = topo_constr(mdl, PDB.RseqRes, PDB.RseqRes, atmDict, resDict);

	// Store required edges
	// 1) Create an ordered list of the desired edges: edge_iterator in topology does not visit the edges in the correct order
	typedef std::pair<int,int> atm_t;
	std::list< std::pair<atm_t,atm_t> > edge_descr_list;
	int n_id  = atmDict.string_to_id[" N  "];
	int ca_id = atmDict.string_to_id[" CA "];
	int c_id  = atmDict.string_to_id[" C  "];
	for(unsigned int n = 0; n < 4; ++n){
		atm_t atmpos1 = std::make_pair( first_res+n, n_id );
		atm_t atmpos2 = std::make_pair( first_res+n, ca_id );
		atm_t atmpos3 = std::make_pair( first_res+n, c_id );
		edge_descr_list.push_back( std::make_pair( atmpos1,atmpos2 ) );
		edge_descr_list.push_back( std::make_pair( atmpos2,atmpos3 ) );
	}
	std::map< std::pair<atm_t,atm_t>, topology::edge_iterator > edge_descr_map;
	
	// 2) Iterate on all edges and save only the required ones
	topology::edge_iterator e_beg, e_end;
	boost::tie(e_beg, e_end) = boost::edges( topo.getGraph() );
	topology::vertex_t u,v;
	unsigned int idx = 0;
	for( topology::edge_iterator e = e_beg; e!=e_end; ++e ){
		v = boost::target(*e, topo.getGraph());
		topo.getGraph()[v].atom->chainId;

	 	if( topo.getGraph()[v].atom->chainId == chain_id ){
	 		u = boost::source(*e, topo.getGraph());
	 		unsigned int ures = topo.getGraph()[u].atom->resSeq, vres = topo.getGraph()[v].atom->resSeq;
	 		unsigned int uatm = topo.getGraph()[u].atom->id, vatm = topo.getGraph()[v].atom->id;
	 		
	 		atm_t atmpos1 = std::make_pair( ures,uatm );
	 		atm_t atmpos2 = std::make_pair( vres,vatm );
	 		std::pair< atm_t, atm_t > edge_descr = std::make_pair( atmpos1,atmpos2 );
	 		
	 		if( find( edge_descr_list.begin(), edge_descr_list.end(), edge_descr ) != edge_descr_list.end() ){
	 			edge_descr_map[ edge_descr ] = e;
	 			++idx;
	 		}
	 	}
	}
	if( idx != N ){
		std::cout << "Found " << idx << " edges istead of " << N << std::endl;
		return 1;
	}
	
	// 3) Save the required edges in an array. Push them in the correct order 
	idx=0;
	std::array< topology::edge_iterator,N> pivot_edges;
	for( std::list< std::pair<atm_t,atm_t> >::iterator d = edge_descr_list.begin(); d != edge_descr_list.end(); ++d ){
		pivot_edges[idx++] = edge_descr_map[*d];
	}
	std::array< topology::edge_iterator,N-1> p1, p2;
	std::array< topology::edge_iterator,N-1>::iterator pivot_last  = pivot_edges.end()  ; --pivot_last ;
	unsigned int i = 0;
	for( std::array< topology::edge_iterator,N-1>::iterator it = pivot_edges.begin(); it!= pivot_edges.end(); ++it, ++i){
		if(it!=pivot_last){
			p1[i] = *it;
		}
		if(it!=pivot_edges.begin()){
			p2[i-1] = *it;
		}
	}
	
	// Construct the corresponding DH chain 
	edges_to_raw_chain<N-1,double,topology> e2c(topo);
	DH::chain<N-1,double>::raw_chain_t raw_chain_1 = e2c(p1), raw_chain_2 = e2c(p2);

	// Initialize Crankshaft algorithm
	lcbc<N-1,double>::parameter_list_t param_list;
	for(unsigned int i = 0; i < param_list.size(); ++i){
		if(i==hard_dof){
			param_list[i] = DH::parameter_t<double>(DH::parameter_id::THETA,i, hardness);
		}
		else{
			param_list[i] = DH::parameter_t<double>(DH::parameter_id::THETA,i, 1.);
		}
	}
	lcbc<N-1,double> cr1( raw_chain_1,param_list ), cr2(raw_chain_2, param_list);
	cr1.setStep(1);
	cr2.setStep(1);
	if(cr1.isSuccess()==false or cr2.isSuccess()==false){
		std::cout << "The system is degenerate!" << std::endl;
		return 1;
	}

	// Move!
	double ds = 0.01;
	if( bond==0 ){
		manifold7dof_visitor<N-1,double> visitor_1(mdl,topo,p1,ds,param_list,eleDict,atmDict,resDict);
		visitor_1.proj_dir = lcbc<N-1,double>::config_t::Zero();
		visitor_1.proj_dir(3) = 1.;
		while( visitor_1.go_on ){
			cr1.step( visitor_1.proj_dir, ds, visitor_1 );
		}
	}
	else if( bond==1 ){
		manifold7dof_visitor<N-1,double> visitor_2(mdl,topo,p2,ds,param_list,eleDict,atmDict,resDict);
		visitor_2.proj_dir = lcbc<N-1,double>::config_t::Zero();
		visitor_2.proj_dir(3) = 1.;
		while( visitor_2.go_on ){
			cr2.step( visitor_2.proj_dir, ds, visitor_2 );
		}
	}	
	return 0;
}
