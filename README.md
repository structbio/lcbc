# Locally Constrained Backbone Configuration

**LCBC** software allow users to change some degrees of freedom of an arbitrary 
protein backbone fragment. Atoms outside this region are not affected by the 
configuration change and no other degrees of freedom are modified.

The algorithm is very general:

+ *bond length*, *bond angles* and *torsional angles* and every combination of 
these can be used as degrees of freedom
+ some addictional variables, such as *r^3* and *cos(theta)* (where *r* is a 
bond length and *theta* is a torsional angle) can be used as degrees of freedom
+ *detailed balance* is satisfied, allowing the algorithm to be easily 
integrated in a Monte Carlo simulation
+ *All* the configurations compatible with the constrains can be efficiently 
visited


## Licensing

**If you use lcbc in your work please cite: 

An efficient algorithm to perform local concerted movements of a chain molecule

S Zamuner, A Rodriguez, F Seno, A Trovato

PloS one 10 (3), e0118342
